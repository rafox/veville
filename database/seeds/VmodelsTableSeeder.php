<?php

use Illuminate\Database\Seeder;

class VmodelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vmodels')->insert([
            'title' => 'BM X5 AUTOMATIQUE',
            'passagers' => 5,
            'bags' => 4,
            'doors' => 5,
            'emission_level' => 169,
            'transmission' => 'a',
            'photo' => '/images/uploads/vmodels/36915_GWY_R.png',
            'brand_id' => 6,
        ]);

        DB::table('vmodels')->insert([
            'title' => 'MERCEDES CLASSE C CABRIOLET AUTOMATIQUE',
            'passagers' => 4,
            'bags' => 1,
            'doors' => 2,
            'emission_level' => 133,
            'transmission' => 'a',
            'photo' => '/images/uploads/vmodels/31759_GWY_R.png',
            'brand_id' => 8,
        ]);

        DB::table('vmodels')->insert([
            'title' => 'MERCEDES CLASSE E CABRIOLET AUTOMATIQUE',
            'passagers' => 4,
            'bags' => 2,
            'doors' => 2,
            'emission_level' => 123,
            'transmission' => 'a',
            'photo' => '/images/uploads/vmodels/43378_GWY_R.png',
            'brand_id' => 8,
        ]);


        DB::table('vmodels')->insert([
            'title' => 'MERCEDES GLE 300D',
            'passagers' => 5,
            'bags' => 4,
            'doors' => 5,
            'emission_level' => 159,
            'transmission' => 'm',
            'photo' => '/images/uploads/vmodels/43375_GWY_R.png',
            'brand_id' => 8,
        ]);

        DB::table('vmodels')->insert([
            'title' => 'AUDI A5SB TFSI STRONIC',
            'passagers' => 5,
            'bags' => 3,
            'doors' => 4,
            'emission_level' => 128,
            'transmission' => 'm',
            'photo' => '/images/uploads/vmodels/41179_GWY_R.png',
            'brand_id' => 1,
        ]);

        DB::table('vmodels')->insert([
            'title' => 'AUDI A4 AUTO',
            'passagers' => 5,
            'bags' => 4,
            'doors' => 4,
            'emission_level' => 111,
            'transmission' => 'm',
            'photo' => '/images/uploads/vmodels/33398_GWY_R.png',
            'brand_id' => 1,
        ]);

        DB::table('vmodels')->insert([
            'title' => 'SUBARU 2019 IMPREZA PREMIUM SEDAN',
            'passagers' => 5,
            'bags' => 4,
            'doors' => 4,
            'emission_level' => 138,
            'transmission' => 'm',
            'photo' => '/images/uploads/vmodels/11201_024_M2Y.png',
            'brand_id' => 5,
        ]);

        DB::table('vmodels')->insert([
            'title' => 'SUBARU 2019 LEGACY 2.5I SPORT',
            'passagers' => 5,
            'bags' => 4,
            'doors' => 4,
            'emission_level' => 128,
            'transmission' => 'm',
            'photo' => '/images/uploads/vmodels/11201_024_M2Y.png',
            'brand_id' => 5,
        ]);

        DB::table('vmodels')->insert([
            'title' => 'TESLA 2019 MODEL S',
            'passagers' => 5,
            'bags' => 5,
            'doors' => 4,
            'emission_level' => 68,
            'transmission' => 'a',
            'photo' => '/images/uploads/vmodels/tesla_s.png',
            'brand_id' => 10,
        ]);

        DB::table('vmodels')->insert([
            'title' => 'TESLA 2019 MODEL 3',
            'passagers' => 5,
            'bags' => 5,
            'doors' => 4,
            'emission_level' => 65,
            'transmission' => 'a',
            'photo' => '/images/uploads/vmodels/tesla_3.png',
            'brand_id' => 10,
        ]);

    }
}
