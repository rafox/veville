<?php

use Illuminate\Database\Seeder;

class AgenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agencies')->insert([
            'title' => 'Agence de Paris',
            'address' => '300 Boulevard de Vaugirard',
            'city' => 'Paris',
            'zip' => '75015',
            'photo' => '/images/uploads/agencies/paris.jpg',
        ]);

        DB::table('agencies')->insert([
            'title' => 'Agence de Bordeaux',
            'address' => '10 place de l\'hôtel de ville',
            'city' => 'Bordeaux',
            'zip' => '33005',
            'photo' => '/images/uploads/agencies/bordeaux.jpg',
        ]);

        DB::table('agencies')->insert([
            'title' => 'Agence de Lyon',
            'address' => '135 rue Sainte-Catherine',
            'city' => 'Lyon',
            'zip' => '69003',
            'photo' => '/images/uploads/agencies/lyon.jpg',
        ]);

        
        DB::table('agencies')->insert([
            'title' => 'Agence de Toulouse',
            'address' => '70 avenue du Petit Bois',
            'city' => 'Toulouse',
            'zip' => '31002 ',
            'photo' => '/images/uploads/agencies/toulouse.jpg',
        ]);

        DB::table('agencies')->insert([
            'title' => 'Agence de Étretat',
            'address' => '4 avenue du remblet',
            'city' => 'Étretat',
            'zip' => '76790  ',
            'photo' => '/images/uploads/agencies/etretat.jpg',
        ]);
    }
}
