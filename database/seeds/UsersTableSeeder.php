<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //add admin user
        $admin = Config::get('users.admin');
        DB::table('users')->insert([
            'username' => ucfirst($admin["username"]),
            'name' => ucfirst(Str::random(10)),
            'first_name' => ucfirst(Str::random(10)),
            'email' => $admin["mail"],
            'email_verified_at' => date("Y-m-d H:i:s"),
            'password' => Hash::make($admin["password"]),
            'gender' => 'w',
            'isAdmin' => 1,
            'isBanned' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        //add some normal users
        $slave = Config::get('users.slave');
        for ($i=0; $i < 3 ; $i++) { 
            DB::table('users')->insert([
                'username' => ucfirst($slave["username"].$i),
                'name' => ucfirst(Str::random(10)),
                'first_name' => ucfirst(Str::random(10)),
                'email' => $slave["username"].$i.'@gmx.fr',
                'email_verified_at' => date("Y-m-d H:i:s"),
                'password' => Hash::make($slave["password"]),
                'gender' => 'w',
                'isAdmin' => 0,
                'isBanned' => 0,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
        
        //add a banned user
        DB::table('users')->insert([
            'username' => ucfirst('slave3'),
            'name' => ucfirst(Str::random(10)),
            'first_name' => ucfirst(Str::random(10)),
            'email' => 'slave3@gmx.fr',
            'email_verified_at' => date("Y-m-d H:i:s"),
            'password' => Hash::make($slave["password"]),
            'gender' => 'w',
            'isAdmin' => 1,
            'isBanned' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

    }
}
