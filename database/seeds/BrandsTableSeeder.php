<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            'title' => 'Audi',
            'photo' => '/images/uploads/brands/audi.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Jeep',
            'photo' => '/images/uploads/brands/jeep.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Chevrolet',
            'photo' => '/images/uploads/brands/chevrolet.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Jaguar',
            'photo' => '/images/uploads/brands/jaguar.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Subaru',
            'photo' => '/images/uploads/brands/subaru.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Bmw',
            'photo' => '/images/uploads/brands/bmw.png',
        ]);
        
        DB::table('brands')->insert([
            'title' => 'Ford Mustang',
            'photo' => '/images/uploads/brands/mustang.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Mercedes-Benz',
            'photo' => '/images/uploads/brands/mercedes.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Porsche',
            'photo' => '/images/uploads/brands/porsche.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Tesla',
            'photo' => '/images/uploads/brands/tesla.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Maserati',
            'photo' => '/images/uploads/brands/maserati.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Land Rover',
            'photo' => '/images/uploads/brands/land_rover.png',
        ]);

        DB::table('brands')->insert([
            'title' => 'Mini',
            'photo' => '/images/uploads/brands/mini.png',
        ]);
    }
}
