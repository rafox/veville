<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);

Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
 });

//Front Office
Route::get('/', 'HomeController@index')->name('home');
Route::get('/mentions-legales', 'HomeController@legales')->name('mentions-legales');
Route::get('/cgv', 'HomeController@cgv')->name('cgv');
Route::get('/banned', 'BannedController@index')->name('banned');


Route::post('/search', 'VehicleController@search')->name('get_vehicles');
Route::get('/search/{agency_id}/{date_time_departure}/{date_time_end}/{order}',
            'VehicleController@search')->name('get_vehicles_order');


Route::get('/order/store/{vehicle_id}/{date_time_departure}/{date_time_end}',
'OrderController@store')->name('order_store');

Route::get('/account', 'UserController@index')->name('account');
Route::post('/account/{id}', 'UserController@update')->name('update_account');

Route::get('/orders', 'OrderController@index')->name('orders');


Route::post('/contact', 'ContactController@send')->name('contact');
Route::get('/contact', 'ContactController@index')->name('contact');

//For test
Route::get('/crash_test', function(){
    $users = Config::get('users');
    dd($users);

});

//Back Office
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
     // Agencies
     Route::get('/agencies', 'AgencyController@index')->name('agencies_index');
     Route::get('/agencies/create', 'AgencyController@create')->name('agencies_create');
     Route::post('/agencies/store', 'AgencyController@store')->name('agencies_store');
     Route::get('/agencies/edit/{id}', 'AgencyController@edit')->name('agencies_edit');
     Route::post('/agencies/update/{id}', 'AgencyController@update')->name('agencies_update');
     Route::get('/agencies/destroy/{id}', 'AgencyController@destroy')->name('agencies_destroy');
     // Users
     Route::get('/', 'UserController@index')->name('home');
     Route::get('/users', 'UserController@index')->name('users_index');
     Route::get('/users/create', 'UserController@create')->name('user_create');
     Route::post('/users/store', 'UserController@store')->name('user_store');
     Route::get('/users/edit/{id}', 'UserController@edit')->name('user_edit');
     Route::post('/users/update/{id}', 'UserController@update')->name('user_update');
     Route::get('/users/destroy/{id}', 'UserController@destroy')->name('user_destroy');
     // Brands
     Route::get('/brands', 'BrandController@index')->name('brands_index');
     Route::get('/brands/create', 'BrandController@create')->name('brands_create');
     Route::post('/brands/store', 'BrandController@store')->name('brands_store');
     Route::get('/brands/edit/{id}', 'BrandController@edit')->name('brands_edit');
     Route::post('/brands/update/{id}', 'BrandController@update')->name('brands_update');
     Route::get('/brands/destroy/{id}', 'BrandController@destroy')->name('brands_destroy');
    // Vmodels
     Route::get('/models', 'VmodelController@index')->name('vmodels_index');
     Route::get('/models/create', 'VmodelController@create')->name('vmodels_create');
     Route::post('/models/store', 'VmodelController@store')->name('vmodels_store');
     Route::get('/models/edit/{id}', 'VmodelController@edit')->name('vmodels_edit');
     Route::post('/models/update/{id}', 'VmodelController@update')->name('vmodels_update');
     Route::get('/models/destroy/{id}', 'VmodelController@destroy')->name('vmodels_destroy');
     // Vehicles
     Route::get('/vehicles', 'VehicleController@index')->name('vehicles_index');
     Route::get('/vehicles/create', 'VehicleController@create')->name('vehicles_create');
     Route::post('/vehicles/store', 'VehicleController@store')->name('vehicles_store');
     Route::get('/vehicles/edit/{id}', 'VehicleController@edit')->name('vehicles_edit');
     Route::post('/vehicles/update/{id}', 'VehicleController@update')->name('vehicles_update');
     Route::get('/vehicles/destroy/{id}', 'VehicleController@destroy')->name('vehicles_destroy');
     // Orders
     Route::get('/orders', 'OrderController@index')->name('orders_index');
     Route::get('/orders/create', 'OrderController@create')->name('orders_create');
     Route::post('/orders/store', 'OrderController@store')->name('orders_store');
     Route::get('/orders/edit/{id}', 'OrderController@edit')->name('orders_edit');
     Route::post('/orders/update/{id}', 'OrderController@update')->name('orders_update');
     Route::get('/orders/destroy/{id}', 'OrderController@destroy')->name('orders_destroy');

});