<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg" color="#4e91e4">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:image:width" content="806">
    <meta property="og:title" content="V&eacute;ville">
    <meta property="og:description" content="V&eacute;ville, location de v&eacute;hicules. Avec une incroyable flotte constitu&eacute;e de marques telles que BMW ou Maserati, les derniers mod&egrave;les de voitures de sport, cabriolets et jeeps...">
    <meta property="og:url" content="https://veville.rafox.ovh/">
    <meta property="og:image" content="https://veville.rafox.ovh/images/og-image.jpg">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
    @include('partials.header')
        <main>
        @include('partials.hello')
            @yield('content')
        </main>
        @include('partials.footer')
    </div>
</body>
</html>
