@extends('layouts.app')

@section('content')
<div class="container">
    <div class="siimple-content siimple-content--large">
        <div class="siimple-h2 siimple--text-normal siimple--mb-1" align="center">Mes Commandes</div>
        <div class="siimple-tabs siimple-tabs--boxed siimple--mt-3">
            <a href="{{ route('account') }}" class="siimple-tabs-item">Mon Compte</a>
            <a href="{{ route('orders') }}" class="siimple-tabs-item siimple-tabs-item--selected">Mes Commandes</a>
        </div>
    @include('partials.notif')
    <div class="siimple-h4">Ma dernière commande</div>
    @if(empty($last_order)) <div align="center">Aucune commande </div> @else
    @include('partials.order', ['order' => $last_order])
    @endif
      <div class="siimple-h4">Mes commandes à venir</div>
      @if($comming_orders->isEmpty()) <div align="center">Aucune commande à venir</div> @endif
      <div class="orders">
        @foreach ($comming_orders as $order)
           @include('partials.order')
        @endforeach    
      </div>

  <div class="siimple-h4">Mes anciennes commandes</div>
  @if($previous_orders->isEmpty()) <div align="center">Aucune ancienne commande</div> @endif
  <div class="orders">
    @foreach ($previous_orders as $order)
      @include('partials.order')
    @endforeach
  </div>

    </div>
</div>
@endsection
