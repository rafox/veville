<div class="siimple-h4">Nos Agences</div>
<div class="home_agencies">
  @foreach ($agencies as $agency)
      <div class="agency" >
        <div class="image">
          <img src="{{ $agency->photo }}" alt="{{ $agency->title }}">
        </div>
        <div class="title">
        {{ $agency->title }}
        </div>
      </div>
  @endforeach
</div>