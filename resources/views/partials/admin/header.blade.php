<header>
<nav class="siimple-navbar siimple-navbar--dark admin">
        <a class="siimple-navbar-title" href="{{ url('/') }}">
        <i class="fas fa-car siimple--color-primary"></i> {{ config('app.name', 'Laravel') }}<span class="admin_span">Dashboard<span>
        </a>
        <div class="siimple--float-right">
        <a href="#" class="menu_btn siimple--color-white" id="menu_btn"><i class="fas fa-bars"></i></a>
        </div>
</nav>
<div class="siimple--bg-dark siimple--color-white menu admin hide" id="menu">
        @guest
            <a class="siimple-btn siimple-btn--blue" href="{{ route('login') }}">{{ __('Se connecter') }}</a>
        @if (Route::has('register'))
            <a class="siimple-btn siimple-btn--green" href="{{ route('register') }}">{{ __('S\'inscrire') }}</a>
        @endif
        @else
            <a href="{{ route('logout') }}" class="siimple-btn siimple-btn--grey" title="{{ __('Logout') }}"><i class="fas fa-sign-out-alt"></i></a>
            <a href="{{ route('account') }}" class="account_btn" title="{{ __('Account') }}"><span class="txt">{{ Auth::user()->username }}</span><span><img src="https://api.adorable.io/avatars/150/{{ Auth::user()->email }}" alt=""></span></a>
            @if(Auth::user()->isAdmin == 1)
            <a href="{{ route('admin.home') }}" class="siimple-btn siimple-btn--orange" title="{{ __('Dashboard') }}"><i class="fas fa-tools"></i></a>
            @endif
            <a href="{{ route('contact') }}" class="siimple-btn siimple-btn--blue" title="{{ __('Contact') }}"><i class="fas fa-at"></i></a>
        @endguest
</div>
</header>
