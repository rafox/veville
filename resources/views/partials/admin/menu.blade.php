<div class="siimple-list siimple-list--hover siimple--text-center">
    <a href="{{ route('admin.users_index') }}" class="siimple-list-item"><span class="siimple-list-title">Utilisateurs</span></a>
    <a href="{{ route('admin.agencies_index') }}"class="siimple-list-item"><span class="siimple-list-title">Agences</span></a>
    <a href="{{ route('admin.brands_index') }}" class="siimple-list-item"><span class="siimple-list-title">Marques</span></a>
    <a href="{{ route('admin.vmodels_index') }}" class="siimple-list-item"><span class="siimple-list-title">Modèles</span></a>
    <a href="{{ route('admin.vehicles_index') }}" class="siimple-list-item"><span class="siimple-list-title">Véhicules</span></a>
    <a href="{{ route('admin.orders_index') }}" class="siimple-list-item"><span class="siimple-list-title">Commandes</span></a>
</div>