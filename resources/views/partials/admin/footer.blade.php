<footer>
<div class="siimple-footer siimple-footer--extra-large siimple-footer--light">
    <div class="siimple-grid-row">
        <div class="siimple-grid-col siimple-grid-col--8">
            <div class="siimple-footer-title">{{ config('app.name', 'Laravel') }}</div>
            <div class="siimple-footer-subtitle"><address>300 Boulevard de Vaugirard, 75015, Paris, France</address></div>
        </div>
    </div>
</div>
</footer>