<div class="order">
        <div class="bloc">
        <div class="photo"><img src="{{ $order->vehicle->vmodel->photo }}" alt="{{ $order->vehicle->vmodel->title }}">
        <div class="logo"><img src="{{ $order->vehicle->vmodel->brand->photo }}" alt="{{ $order->vehicle->vmodel->brand->title }}"></div>

        </div>
        <div class="infos">
          <div class="vehicle_title">{{ $order->vehicle->vmodel->title }}</div>
          <div class="start"><i class="fas fa-calendar siimple--color-success"></i> Date de départ : {{ $order->date_time_departure }}</div>
          <div class="end"><i class="fas fa-calendar siimple--color-warning"></i> Date de retour : {{ $order->date_time_end }}</div>
          <div class="price"><i class="far fa-money-bill-wave siimple--color-warning"></i> {{ $order->total_price }} €</div>
        </div>
        </div>
        <div class="agency_title">{{ $order->agency->title }}</div>
        <div class="agency" style="background-image: linear-gradient(90deg, rgba(84,103,120,1) 0%, rgba(84,103,120,1) 75%, rgba(84,103,120,0.5049370089832808) 100%, rgba(2,0,36,1) 546778%), url('{{ $order->agency->photo }}');"></div>

      </div>