<div class="vehicle">
        <div class="bloc">
        <div class="photo"><img src="{{ $vehicle->vmodel->photo }}" alt="{{ $vehicle->vmodel->title }}">
        <div class="logo"><img src="{{ $vehicle->vmodel->brand->photo }}" alt="{{ $vehicle->vmodel->brand->title }}"></div>

        </div>
        <div class="infos">
          <div class="vehicle_title">{{ $vehicle->vmodel->title }}</div>
          <div class="desc">
            <div>
            <span><i class="fas fa-users"></i> {{ $vehicle->vmodel->passagers }} Passagers</span> / 
            <span><i class="fas fa-door-open"></i> {{ $vehicle->vmodel->doors }} Portes</span> / 
            <span><i class="fas fa-briefcase"></i> {{ $vehicle->vmodel->bags }} Bagages</span>
            </div>
            <div>
            <span><i class="fas fa-inhaler"></i> {{ $vehicle->vmodel->emission_level }} g/100km</span> / 
            <span><i class="fas fa-cogs"></i> @if($vehicle->vmodel->transmission == "m") Manuelle @endif
                @if($vehicle->vmodel->transmission == "a") Automatique @endif
            </span>
            </div>
          </div>
          <div class="price"><i class="far fa-money-bill-wave siimple--color-warning"></i> {{ $vehicle->daily_price }} € par jour</div>
          <div class="price"><i class="far fa-money-bill-wave siimple--color-warning"></i> Prix total : {{ $vehicle->daily_price * $days }} €</div>

        </div>
        </div>
        <div class="agency_title">{{ $vehicle->agency->title }}</div>
        <div class="agency" style="background-image: linear-gradient(90deg, rgba(84,103,120,1) 0%, rgba(84,103,120,1) 75%, rgba(84,103,120,0.5049370089832808) 100%, rgba(2,0,36,1) 546778%), url('{{ $vehicle->agency->photo }}');"></div>
        <a class="order_button" href="{{ route('order_store', [
                        $vehicle->id,
                        $date_time_departure,
                        $date_time_end
                        ]) }}">Réserver</a>
      </div>