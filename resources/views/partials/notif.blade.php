@if (session('status'))
    <div class="siimple-alert siimple-alert--success" role="alert">
        {{ session('status') }}
    </div>
@endif

@if(session('success'))
    <div class="siimple-alert siimple-alert--success">
        {{ session('success') }}
    </div>
@endif

@if(session('error'))
    <div class="siimple-alert siimple-alert--error">
        {{ session('error') }}
    </div>
@endif

@if ($errors->any())
    <div class="siimple-alert siimple-alert--error">
        @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </div>
 @endif

 @if (session('resent'))
    <div class="siimple-alert siimple-alert--success">
        {{ __('A fresh verification link has been sent to your email address.') }}
    </div>
@endif