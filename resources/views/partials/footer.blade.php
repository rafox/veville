<footer>
<div class="siimple-footer siimple-footer--extra-large siimple-footer--dark">
    <div class="siimple-grid-row">
        <div class="siimple-grid-col siimple-grid-col--8">
            <div class="siimple-footer-title">{{ config('app.name', 'Laravel') }}</div>
            <div class="siimple-footer-subtitle"><address>300 Boulevard de Vaugirard, 75015, Paris, France</address></div>
        </div>
        <div class="siimple-grid-col siimple-grid-col--4">
            <a href="{{ route('mentions-legales') }}" class="siimple-footer-link">Mentions légales</a>
            <a href="{{ route('cgv') }}" class="siimple-footer-link">Cgv</a>
        </div>
    </div>
</div>
</footer>