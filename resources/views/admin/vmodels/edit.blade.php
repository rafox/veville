@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    @include('partials.admin.notif')
                    <form method="POST" action="{{ route('admin.vmodels_update', [$vmodel->id]) }}" enctype="multipart/form-data">
                    @csrf

                    <div class="siimple-form">
                        <div class="siimple-form-title">Modifier un Modèle</div>
                        
                        <div class="siimple-form-field">
                          <label class="siimple-form-field-label">Marque: </label> 
                          <select name="brand_id" class="siimple-select siimple-select--fluid">
                            @foreach ($brands as $brand)
                              <option value="{{ $brand->id }}" @if($vmodel->brand->id == $brand->id) selected @endif>{{ $brand->title }}</option> 
                            @endforeach
                          </select>
                        </div>


                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Titre</div>
                            <input type="text" name="title" class="siimple-input siimple-input--fluid" value="{{ $vmodel->title }}">
                        </div>
                        
                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Passagers</div>
                            <select name="passagers" class="siimple-select siimple-select--fluid">
                              @for ($i = 1; $i < 10; $i++)
                                <option value="{{ $i }}" @if($vmodel->passagers == $i) selected @endif>{{ $i }} Passagers</option> 
                              @endfor 
                              </select>                       
                            </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Coffres</div>
                            <select name="bags" class="siimple-select siimple-select--fluid">
                              @for ($i = 0; $i < 6; $i++)
                                <option value="{{ $i }}" @if($vmodel->bags == $i) selected @endif>{{ $i }} Coffres</option> 
                              @endfor
                              </select>                        
                            </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Portes</div>
                            <select name="doors" class="siimple-select siimple-select--fluid">
                              @for ($i = 1; $i < 6; $i++)
                                <option value="{{ $i }}" @if($vmodel->doors == $i) selected @endif>{{ $i }} Portes</option> 
                              @endfor
                              </select>                           
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Taux d'émission</div>
                            <select name="emission_level" class="siimple-select siimple-select--fluid">
                              @for ($i = 1; $i < 251; $i++)
                                <option value="{{ $i }}" @if($vmodel->doors == $i) selected @endif>{{ $i }}</option> 
                              @endfor
                              </select>                           
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Transmission</div>
                            <select name="transmission" class="siimple-select siimple-select--fluid">
                                <option value="m" @if($vmodel->doors == "m") selected @endif>Manuelle</option> 
                                <option value="a" @if($vmodel->doors == "a") selected @endif>Automatique</option> 

                              </select>                           
                        </div>

                        <div class="siimple-field">
                            <label class="siimple-form-field-label">Photo</label>
                            <input type="file" name="image" class="siimple-input siimple-input--fluid">
                        </div>

                        <div class="form-field">
                            <input type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid" value="Envoyer">
                        </div>
                    </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
