@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    <div class="bloc_add"><a href="{{ route('admin.vmodels_create') }}" class="siimple-btn siimple-btn--blue"><i class="fas fa-plus-circle"></i> Créer</a></div>
                    @include('partials.admin.notif')
                    <div class="admin_table">
                    <div class="siimple-table">
                      <div class="siimple-table-header">
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell siimple-table-cell--1">ID</div>
                              <div class="siimple-table-cell siimple-table-cell--1">Marque</div>
                              <div class="siimple-table-cell siimple-table-cell--1">Photo</div>
                              <div class="siimple-table-cell siimple-table-cell--8">Titre</div>
                              <div class="siimple-table-cell siimple-table-cell--8">Passagers</div>
                              <div class="siimple-table-cell siimple-table-cell--8">Coffres</div>
                              <div class="siimple-table-cell siimple-table-cell--8">Portes</div>
                              <div class="siimple-table-cell siimple-table-cell--8">Taux d'émission</div>
                              <div class="siimple-table-cell siimple-table-cell--8">Transmission</div>
                              <div class="siimple-table-cell">Modifier</div>
                              <div class="siimple-table-cell">Supprimer</div>

                          </div>
                      </div>
                      <div class="siimple-table-body">
                        @foreach ($vmodels as $vmodel)
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">{{ $vmodel->id }}</div>
                              <div class="siimple-table-cell"><span class="tab_img brand"><img src="{{ $vmodel->brand->photo }}" alt="{{ $vmodel->brand->name }}" ></span></div>
                              <div class="siimple-table-cell"><span class="tab_img brand"><img src="{{ $vmodel->photo }}" alt="img_{{ $vmodel->id }}" ></span></div>
                              <div class="siimple-table-cell">{{ $vmodel->title }}</div>
                              <div class="siimple-table-cell">{{ $vmodel->passagers }}</div>
                              <div class="siimple-table-cell">{{ $vmodel->bags }}</div>
                              <div class="siimple-table-cell">{{ $vmodel->doors }}</div>
                              <div class="siimple-table-cell">{{ $vmodel->emission_level }} g/km</div>
                              <div class="siimple-table-cell">@if($vmodel->transmission == 'm') Manuelle @elseif($vmodel->transmission == 'a') Automatique @endif</div>

                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--green" href="{{ route('admin.vmodels_edit', [$vmodel->id]) }}"><i class="fas fa-edit"></i></a></div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--red" href="{{ route('admin.vmodels_destroy', [$vmodel->id]) }}"><i class="fas fa-trash-alt"></i></a></div>
                          </div>
                        @endforeach
                      </div>
                      </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
