@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    <div class="bloc_add"><a href="{{ route('admin.user_create') }}" class="siimple-btn siimple-btn--blue"><i class="fas fa-plus-circle"></i> Créer</a></div>
                    @include('partials.admin.notif')
                    <div class="admin_table">
                    <div class="siimple-table">
                      <div class="siimple-table-header">
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">ID</div>
                              <div class="siimple-table-cell">Avatar</div>
                              <div class="siimple-table-cell">Username</div>
                              <div class="siimple-table-cell">Nom</div>
                              <div class="siimple-table-cell">Prénom</div>
                              <div class="siimple-table-cell">Email</div>
                              <div class="siimple-table-cell">Genre</div>
                              <div class="siimple-table-cell">Admin</div>
                              <div class="siimple-table-cell">Banni</div>
                              <div class="siimple-table-cell">Créé à</div>
                              <div class="siimple-table-cell">Modifier</div>
                              <div class="siimple-table-cell">Supprimer</div>

                          </div>
                      </div>
                      <div class="siimple-table-body">
                        @foreach ($users as $user)
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">{{ $user->id }}</div>
                              <div class="siimple-table-cell"><span class="tab_img"><img src="https://api.adorable.io/avatars/150/{{ $user->email }}" alt="img_{{ $user->id }}" ></span></div>

                              <div class="siimple-table-cell">{{ $user->username }}</div>
                              <div class="siimple-table-cell">{{ $user->name }}</div>
                              <div class="siimple-table-cell">{{ $user->first_name }}</div>
                              <div class="siimple-table-cell">{{ $user->email }}</div>
                              <div class="siimple-table-cell">{{ $user->gender }}</div>
                              <div class="siimple-table-cell">{{ $user->isAdmin }}</div>
                              <div class="siimple-table-cell">{{ $user->isBanned }}</div>
                              <div class="siimple-table-cell"><span class="tab_date">{{ $user->created_at }}</span></div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--green" href="{{ route('admin.user_edit', [$user->id]) }}"><i class="fas fa-edit"></i></a></div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--red" href="{{ route('admin.user_destroy', [$user->id]) }}"><i class="fas fa-trash-alt"></i></a></div>
                          </div>
                        @endforeach
                      </div>
                      </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
