@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    @include('partials.admin.notif')
                    <form method="post" action="{{ route('admin.user_store') }}">
                    @csrf
                    <div class="siimple-form">
                        <div class="siimple-form-title">Créer un Utilisateur</div>
                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Nom d'utilisateur</div>
                            <input type="text" name="username" class="siimple-input siimple-input--fluid" value="">
                        </div>
                        
                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Nom</div>
                            <input type="text" name="first_name" class="siimple-input siimple-input--fluid" value="">
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Prénom</div>
                            <input type="text" name="name" class="siimple-input siimple-input--fluid" value="">
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Adresse email</div>
                            <input type="email" name="email" class="siimple-input siimple-input--fluid" value="">
                        </div>

                        <div class="siimple-field">
                            <label for="password" class="siimple-field-label">Mot de passe</label>
                            <input id="password" type="password" class="siimple-input siimple-input--fluid " name="password">
                        </div>

                        <div class="siimple-field">
                            <label for="password-confirm" class="siimple-field-label">Confirmer le mot de passe</label>
                            <input id="password-confirm" type="password" class="siimple-input siimple-input--fluid" name="password_confirmation">
                        </div>
                        

                        <div class="siimple-field">
                            <label for="gender" class="siimple-field-label">Genre</label>
                            <select id="gender" class="siimple-select siimple-select--fluid " name="gender">
                                <option value="m" >Homme</option>
                                <option value="w">Femme</option>
                            </select>                          
                        </div>

                        <div class="siimple-field">
                          <label class="siimple-label">Admin:</label>
                          <div class="siimple-switch">
                              <input type="checkbox" name="isAdmin" id="isAdmin">
                              <label for="isAdmin"></label>
                          </div>

                          <label class="siimple-label">Banni:</label>
                          <div class="siimple-switch siimple-switch--warning">
                              <input type="checkbox" name="isBanned" id="isBanned">
                              <label for="isBanned"></label>
                          </div>
                        </div>


                        <div class="form-field">
                            <input type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid" value="Envoyer">
                        </div>
                    </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
