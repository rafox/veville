@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    <div class="bloc_add"><a href="{{ route('admin.brands_create') }}" class="siimple-btn siimple-btn--blue"><i class="fas fa-plus-circle"></i> Créer</a></div>
                    @include('partials.admin.notif')
                    <div class="admin_table">
                    <div class="siimple-table">
                      <div class="siimple-table-header">
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell siimple-table-cell--1">ID</div>
                              <div class="siimple-table-cell siimple-table-cell--1">Photo</div>
                              <div class="siimple-table-cell siimple-table-cell--8">Titre</div>
                              <div class="siimple-table-cell">Modifier</div>
                              <div class="siimple-table-cell">Supprimer</div>

                          </div>
                      </div>
                      <div class="siimple-table-body">
                        @foreach ($brands as $brand)
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">{{ $brand->id }}</div>
                              <div class="siimple-table-cell"><span class="tab_img brand"><img src="{{ $brand->photo }}" alt="img_{{ $brand->id }}" ></span></div>
                              <div class="siimple-table-cell">{{ $brand->title }}</div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--green" href="{{ route('admin.brands_edit', [$brand->id]) }}"><i class="fas fa-edit"></i></a></div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--red" href="{{ route('admin.brands_destroy', [$brand->id]) }}"><i class="fas fa-trash-alt"></i></a></div>
                          </div>
                        @endforeach
                      </div>
                      </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
