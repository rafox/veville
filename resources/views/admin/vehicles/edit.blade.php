@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    @include('partials.admin.notif')
                    <form method="POST" action="{{ route('admin.vehicles_update', [$vehicle->id]) }}" enctype="multipart/form-data">
                    @csrf

                    <div class="siimple-form">
                        <div class="siimple-form-title">Modifier un Véhicule</div>

                        <div class="siimple-form-field">
                          <label class="siimple-form-field-label">Agence: </label> 
                          <select name="agency_id" class="siimple-select siimple-select--fluid">
                            @foreach ($agencies as $agency)
                              <option value="{{ $agency->id }}" @if($vehicle->agency_id == $agency->id) selected @endif>{{ $agency->title }}</option> 
                            @endforeach
                          </select>
                        </div>

                        <div class="siimple-form-field">
                          <label class="siimple-form-field-label">Modèle: </label> 
                          <select name="vmodel_id" class="siimple-select siimple-select--fluid">
                            @foreach ($vmodels as $vmodel)
                              <option value="{{ $vmodel->id }}" @if($vehicle->vmodel_id == $vmodel->id) selected @endif>{{ $vmodel->title }}</option> 
                            @endforeach
                          </select>
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Prix</div>
                            <input type="text" name="daily_price" class="siimple-input siimple-input--fluid" value="{{ $vehicle->daily_price }}">
                        </div>

                        <div class="form-field">
                            <input type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid" value="Envoyer">
                        </div>
                    </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
