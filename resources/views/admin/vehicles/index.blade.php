@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    <div class="bloc_add"><a href="{{ route('admin.vehicles_create') }}" class="siimple-btn siimple-btn--blue"><i class="fas fa-plus-circle"></i> Créer</a></div>
                    @include('partials.admin.notif')
                    <div class="admin_table">
                    <div class="siimple-table">
                      <div class="siimple-table-header">
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell siimple-table-cell--1">ID</div>
                              <div class="siimple-table-cell siimple-table-cell--1">Marque</div>
                              <div class="siimple-table-cell siimple-table-cell--1">Photo</div>
                              <div class="siimple-table-cell siimple-table-cell--5">Titre</div>
                              <div class="siimple-table-cell siimple-table-cell--1">Agency</div>
                              <div class="siimple-table-cell siimple-table-cell--2"></div>
                              <div class="siimple-table-cell siimple-table-cell--2">Tarif journalier</div>
                              <div class="siimple-table-cell">Modifier</div>
                              <div class="siimple-table-cell">Supprimer</div>

                          </div>
                      </div>
                      <div class="siimple-table-body">
                        @foreach ($vehicles as $vehicle)
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">{{ $vehicle->id }}</div>
                              <div class="siimple-table-cell"><span class="tab_img brand"><img src="{{ $vehicle->vmodel->brand->photo }}" alt="{{ $vehicle->vmodel->brand->name }}" ></span></div>
                              <div class="siimple-table-cell"><span class="tab_img brand"><img src="{{ $vehicle->vmodel->photo }}" alt="img_{{ $vehicle->vmodel->id }}" ></span></div>
                              <div class="siimple-table-cell">{{ $vehicle->vmodel->title }}</div>
                              <div class="siimple-table-cell"><span class="tab_img agency"><img src="{{ $vehicle->agency->photo }}" alt="{{ $vehicle->agency->name }}" ></span></div>
                              <div class="siimple-table-cell">{{ $vehicle->agency->title }}</div>
                              <div class="siimple-table-cell">{{ number_format($vehicle->daily_price, 2) }} €</div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--green" href="{{ route('admin.vehicles_edit', [$vehicle->id]) }}"><i class="fas fa-edit"></i></a></div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--red" href="{{ route('admin.vehicles_destroy', [$vehicle->id]) }}"><i class="fas fa-trash-alt"></i></a></div>
                          </div>
                        @endforeach
                      </div>
                      </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
