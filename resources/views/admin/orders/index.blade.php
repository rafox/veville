@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    <div class="bloc_add"><a href="{{ route('admin.orders_create') }}" class="siimple-btn siimple-btn--blue"><i class="fas fa-plus-circle"></i> Créer</a></div>
                    @include('partials.admin.notif')
                    <div class="admin_table">
                    <div class="siimple-table">
                      <div class="siimple-table-header">
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">ID</div>
                              <div class="siimple-table-cell">Aavatar</div>
                              <div class="siimple-table-cell">Utilisateur</div>
                              <div class="siimple-table-cell siimple-table-cell--3">Véhicule</div>
                              <div class="siimple-table-cell siimple-table-cell--3">Agence</div>
                              <div class="siimple-table-cell siimple-table-cell--2">Date de départ</div>
                              <div class="siimple-table-cell siimple-table-cell--2">Date de retour</div>
                              <div class="siimple-table-cell siimple-table-cell--2">Total</div>
                              <div class="siimple-table-cell">Modifier</div>
                              <div class="siimple-table-cell">Supprimer</div>

                          </div>
                      </div>
                      <div class="siimple-table-body">
                        @foreach ($orders as $order)
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">{{ $order->id }}</div>
                              <div class="siimple-table-cell"><span class="tab_img"><img src="https://api.adorable.io/avatars/150/{{ $order->user->email }}" alt="{{ $order->user->username }}" ></span></div>
                              <div class="siimple-table-cell">{{ $order->user->username }}</div>
                              <div class="siimple-table-cell">{{ $order->vehicle->vmodel->title }}</div>
                              <div class="siimple-table-cell">{{ $order->agency->title }}</div>
                              <div class="siimple-table-cell">{{ $order->date_time_departure }}</div>
                              <div class="siimple-table-cell">{{ $order->date_time_end }}</div>
                              <div class="siimple-table-cell">{{ number_format($order->total_price, 2) }} €</div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--green" href="{{ route('admin.orders_edit', [$order->id]) }}"><i class="fas fa-edit"></i></a></div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--red" href="{{ route('admin.orders_destroy', [$order->id]) }}"><i class="fas fa-trash-alt"></i></a></div>
                          </div>
                        @endforeach
                      </div>
                      </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
