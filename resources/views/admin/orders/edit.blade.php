@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    @include('partials.admin.notif')
                    <form method="POST" action="{{ route('admin.orders_update', [$order->id]) }}" enctype="multipart/form-data">
                    @csrf

                    <div class="siimple-form">
                        <div class="siimple-form-title">Modifier une Commande</div>

                        <div class="siimple-form-field">
                          <label class="siimple-form-field-label">Utilisateur: </label> 
                          <select name="user_id" class="siimple-select siimple-select--fluid">
                            @foreach ($users as $user)
                              <option value="{{ $user->id }}">{{ $user->username }}</option> 
                            @endforeach
                          </select>
                        </div>

                        <div class="siimple-form-field">
                          <label class="siimple-form-field-label">Véhicules: </label> 
                          <select name="vehicle_id" class="siimple-select siimple-select--fluid">
                            @foreach ($vehicles as $vehicle)
                              <option value="{{ $vehicle->id }}">{{ $vehicle->vmodel->title }}</option> 
                            @endforeach
                          </select>
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Date de départ</div>
                            <input type="date" name="date_time_departure" class="siimple-input siimple-input--fluid" value="{{ $order->date_time_departure }}">
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Date de retour</div>
                            <input type="date" name="date_time_end" class="siimple-input siimple-input--fluid" value="{{ $order->date_time_end }}">
                        </div>

                        <div class="form-field">
                            <input type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid" value="Envoyer">
                        </div>
                    </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
