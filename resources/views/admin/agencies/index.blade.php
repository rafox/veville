@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    <div class="bloc_add"><a href="{{ route('admin.agencies_create') }}" class="siimple-btn siimple-btn--blue"><i class="fas fa-plus-circle"></i> Créer</a></div>
                    @include('partials.admin.notif')
                    <div class="admin_table">
                    <div class="siimple-table">
                      <div class="siimple-table-header">
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">ID</div>
                              <div class="siimple-table-cell">Photo</div>
                              <div class="siimple-table-cell">Titre</div>
                              <div class="siimple-table-cell siimple-table-cell--4">Adresse</div>
                              <div class="siimple-table-cell">Ville</div>
                              <div class="siimple-table-cell">Code Postal</div>
                              <div class="siimple-table-cell">Modifier</div>
                              <div class="siimple-table-cell">Supprimer</div>

                          </div>
                      </div>
                      <div class="siimple-table-body">
                        @foreach ($agencies as $agency)
                          <div class="siimple-table-row">
                              <div class="siimple-table-cell">{{ $agency->id }}</div>
                              <div class="siimple-table-cell"><span class="tab_img agency"><img src="{{ $agency->photo }}" alt="img_{{ $agency->id }}" ></span></div>
                              <div class="siimple-table-cell">{{ $agency->title }}</div>
                              <div class="siimple-table-cell">{{ $agency->address }}</div>
                              <div class="siimple-table-cell">{{ $agency->city }}</div>
                              <div class="siimple-table-cell">{{ $agency->zip }}</div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--green" href="{{ route('admin.agencies_edit', [$agency->id]) }}"><i class="fas fa-edit"></i></a></div>
                              <div class="siimple-table-cell"><a class="siimple-btn siimple-btn--red" href="{{ route('admin.agencies_destroy', [$agency->id]) }}"><i class="fas fa-trash-alt"></i></a></div>
                          </div>
                        @endforeach
                      </div>
                      </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
