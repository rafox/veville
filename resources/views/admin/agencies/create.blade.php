@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">

                <div class="card-body">
                    @include('partials.admin.notif')
                    <form method="POST" action="{{ route('admin.agencies_store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="siimple-form">
                        <div class="siimple-form-title">Créer une Agence</div>
                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Titre</div>
                            <input type="text" name="title" class="siimple-input siimple-input--fluid" value="">
                        </div>
                        
                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Adresse</div>
                            <input type="text" name="address" class="siimple-input siimple-input--fluid" value="">
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Ville</div>
                            <input type="text" name="name" class="siimple-input siimple-input--fluid" value="">
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Code Postal</div>
                            <input type="text" name="zip" class="siimple-input siimple-input--fluid" value="">
                        </div>

                        <div class="siimple-field">
                            <label class="siimple-form-field-label">Photo</label>
                            <input type="file" name="image" class="siimple-input siimple-input--fluid">
                        </div>

    


                        <div class="form-field">
                            <input type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid" value="Envoyer">
                        </div>
                    </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
