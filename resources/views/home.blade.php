@extends('layouts.app')

@section('content')
<div class="container">
    <div class="search">
        <form action="{{ route('get_vehicles') }}" method="POST">
        @csrf

    <div class="agency">
    <span class="search_txt">Agence</span>
    <select name="agency_id" class="date_search">
                            @foreach ($agencies as $agency)
                              <option value="{{ $agency->id }}">{{ $agency->title }}</option> 
                            @endforeach
                          </select>
    </div>

<div class="start_date">
    <span class="search_txt">Date de départ</span> <input class="date_search" type="date" name="date_time_departure" value="{{ date("Y-m-d", strtotime('+2 day')) }}">
</div>    
<div class="end_date">
    <span class="search_txt">Date de retour</span> <input class="date_search" type="date" name="date_time_end" value="{{ date("Y-m-d", strtotime('+4 day')) }}">
</div>

<input class="search_btn" type="submit" value="Envoyer">
</form>
    </div>
    <div class="siimple-content siimple-content--large">
            @include('partials.notif')
        @if(isset($vehicles))
        <div class="price_filter">
        Trier par prix
            <div class="siimple-btn-group">
                <a href="{{ route('get_vehicles_order', [
                        $vehicles->first()->agency_id,
                        $date_time_departure,
                        $date_time_end,
                        $order[0]
                    ]) }}" class="siimple-btn siimple-btn--primary"><i class="fas fa-angle-down"></i></a>
                <a href="{{ route('get_vehicles_order', [
                        $vehicles->first()->agency_id,
                        $date_time_departure,
                        $date_time_end,
                        $order[1]
                    ]) }}" class="siimple-btn siimple-btn--primary"><i class="fas fa-angle-up"></i></a>
            </div>
        </div>
            @foreach ($vehicles as $vehicle)
                @include('partials.vehicle')
            @endforeach
        @endif
        @include('partials.agencies')
        @include('partials.brands')
    </div>
</div>
@endsection
