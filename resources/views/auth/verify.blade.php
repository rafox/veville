@extends('layouts.app')

@section('content')
<div class="container">
    <div class="siimple-content siimple-content--extra-small">
        <div class="siimple--px-5 siimple--py-5">
            <div class="card">
                <div class="siimple-h2 siimple--text-normal siimple--mb-1" align="center">{{ __('Vérifiez votre adresse e-mail') }}</div>

                <div class="siimple-paragraph siimple-paragraph--lead">
                @include('partials.notif')


                    {{ __('Avant de continuer, veuillez vérifier votre courrier électronique pour un lien de vérification.') }}
                    {{ __('Si vous n\'avez pas reçu l\'email') }} ? <a class="siimple-btn siimple-btn--primary siimple-btn--fluid siimple--text-bold" href="{{ route('verification.resend') }}">{{ __('Cliquez ici pour demander un autre') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
