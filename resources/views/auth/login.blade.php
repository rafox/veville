@extends('layouts.app')

@section('content')
<div class="container">
    <div class="siimple-content siimple-content--extra-small">
        <div class="siimple-h2 siimple--text-normal siimple--mb-1" align="center">
        S'identifier
        </div>
        <div class="siimple-paragraph siimple-paragraph--lead" align="center">
        Utilisez votre email et votre mot de passe pour vous connecter à notre site
        </div>

        @include('partials.notif')

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="siimple-field">
                    <label for="email" class="siimple-field-label">{{ __('Adresse email') }}</label>


                    <input class="siimple-input siimple-input--fluid {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                    <span class="siimple-field-helper siimple--color-error" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span> @endif

                </div>

                <div class="siimple-field">
                    <label for="password" class="siimple-field-label">{{ __('Mot de passe') }}</label>


                    <input class="siimple-input siimple-input--fluid  {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                    <span class="siimple-field-helper siimple--color-error" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span> @endif

                </div>


                <div class="siimple-field">
                    <button type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid siimple--text-bold">
                                    {{ __('S\'identifier') }}
                                </button>
                </div>
                <div class="siimple-field">
                    <span class="form-check">
                    <div class="siimple-checkbox">
                            <input class="siimple-checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember"></label>
                    </div>
                            <label class="siimple-label" for="remember">
                                {{ __('Se souvenir de moi') }}
                            </label>
                        </span> @if (Route::has('password.request'))
                    <a class="siimple-label" href="{{ route('password.request') }}">
                                    {{ __('Mot de passe oublié ?') }}
                                </a> @endif
                </div>
            </form>
        <div class="siimple-card siimple--mt-5" align="center">
            <div class="siimple-card-body">
            Vous n'avez pas de compte ? <a class="siimple-link" href="{{ route('register') }}">Créer un compte</a>.
            </div>
        </div>
    </div>
</div>
@endsection
