@extends('layouts.app')

@section('content')
<div class="container">
    <div class="siimple-content siimple-content--extra-small">
            <div class="siimple-h2 siimple--text-normal siimple--mb-1" align="center">
                {{ __('Réinitialiser le mot de passe') }}
            </div>

                    @include('partials.notif')
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="siimple-field">
                            <label for="email" class="siimple-field-label">{{ __('Adresse email') }}</label>

                                <input id="email" type="email" class="siimple-input siimple-input--fluid {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        </div>

                        <div class="siimple-field">
                                <button type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid siimple--text-bold">
                                    {{ __('Envoyer le lien de réinitialisation du mot de passe') }}
                                </button>
                        </div>
                    </form>
    </div>
</div>
@endsection
