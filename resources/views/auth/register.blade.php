@extends('layouts.app')

@section('content')
<div class="container">
    <div class="siimple-content siimple-content--extra-small">

            <div class="siimple-h2 siimple--text-normal siimple--mb-1" align="center">
                {{ __('S\'inscrire') }}
            </div>
            @include('partials.notif')

                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="siimple-field">
                            <label for="username" class="siimple-field-label">{{ __('Nom d\'utilisateur') }}</label>

                                <input id="username" type="text" class="siimple-input siimple-input--fluid {{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                        
                            
                        </div>

                        <div class="siimple-field">
                            <label for="email" class="siimple-field-label">{{ __('Adresse email') }}</label>

                                <input id="email" type="email" class="siimple-input siimple-input--fluid {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                        </div>

                        <div class="siimple-field">
                            <label for="password" class="siimple-field-label">{{ __('Mot de passe') }}</label>

                                <input id="password" type="password" class="siimple-input siimple-input--fluid {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        </div>

                        <div class="siimple-field">
                            <label for="password-confirm" class="siimple-field-label">{{ __('Confirmer le mot de passe') }}</label>

                                <input id="password-confirm" type="password" class="siimple-input siimple-input--fluid" name="password_confirmation" required>
                        </div>


                        <div class="siimple-field">
                            <label for="name" class="siimple-field-label">{{ __('Nom') }}</label>

                                <input id="name" type="text" class="siimple-input siimple-input--fluid {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                            
                        </div>


                        <div class="siimple-field">
                            <label for="first_name" class="siimple-field-label">{{ __('Prénom') }}</label>

                                <input id="first_name" type="text" class="siimple-input siimple-input--fluid {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

        
                            
                        </div>

                        <div class="siimple-field">
                            <label for="gender" class="siimple-field-label">{{ __('Genre') }}</label>

                                <select id="gender" class="siimple-select siimple-select--fluid {{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" required autofocus>
                                    <option value="m" selected>Man</option>
                                    <option value="w">Women</option>
                                </select>
                            
                        </div>

                        <div class="siimple-field">
                                <button type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid siimple--text-bold">
                                    {{ __('S\'inscrire') }}
                                </button>
                        </div>
                    </form>
                    <div class="siimple-card siimple--mt-5" align="center">
                        <div class="siimple-card-body">
                        Vous avez un compte ? <a class="siimple-link" href="{{ route('login') }}">Se connecter</a>.
                        </div>
                    </div>
    </div>
</div>
@endsection
