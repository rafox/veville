@extends('layouts.app')

@section('content')
<div class="container">
    <div class="siimple-content siimple-content--large">
        <div class="siimple-h2 siimple--text-normal siimple--mb-1" align="center">Mon Compte</div>
        <div class="siimple-tabs siimple-tabs--boxed siimple--mt-3">
            <a href="{{ route('account') }}" class="siimple-tabs-item siimple-tabs-item--selected">Mon Compte</a>
            <a href="{{ route('orders') }}" class="siimple-tabs-item">Mes Commandes</a>
        </div>
    @include('partials.notif')
                    <form method="post" action="{{ route('update_account', [$user->id]) }}">
                    @csrf
                    <div class="siimple-form">
                        <div class="siimple-form-title">Modifier un Utilisateur</div>
                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Nom d'utilisateur</div>
                            <input type="text" name="username" class="siimple-input siimple-input--fluid" value="{{ $user->username }}">
                        </div>
                        
                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Nom</div>
                            <input type="text" name="first_name" class="siimple-input siimple-input--fluid" value="{{ $user->name }}">
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Prénom</div>
                            <input type="text" name="name" class="siimple-input siimple-input--fluid" value="{{ $user->first_name }}">
                        </div>

                        <div class="siimple-form-field">
                            <div class="siimple-form-field-label">Adresse email</div>
                            <input type="email" name="email" class="siimple-input siimple-input--fluid" value="{{ $user->email }}">
                        </div>

                        <div class="siimple-field">
                            <label for="password" class="siimple-field-label">Mot de passe</label>
                            <input id="password" type="password" class="siimple-input siimple-input--fluid " name="password">
                        </div>

                        <div class="siimple-field">
                            <label for="password-confirm" class="siimple-field-label">Confirmer le mot de passe</label>
                            <input id="password-confirm" type="password" class="siimple-input siimple-input--fluid" name="password_confirmation">
                        </div>
                        

                        <div class="siimple-field">
                            <label for="gender" class="siimple-field-label">Genre</label>
                            <select id="gender" class="siimple-select siimple-select--fluid " name="gender">
                                <option value="m" @if ($user->gender == "m") checked @endif>Homme</option>
                                <option value="w"@if ($user->gender == "w") checked @endif>Femme</option>
                            </select>                          
                        </div>

                        <div class="form-field">
                            <input type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid" value="Envoyer">
                        </div>
                    </div>
                    </form>
                    </div>

</div>
@endsection
