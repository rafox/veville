@extends('layouts.app')

@section('content')
<div class="container">
    <div class="siimple-content siimple-content--large">
    <h1>Mentions légales</h1>
    <hr>

    <div class="w-100 mx-auto text-dark text-center alert alert-danger">Ce site n'a aucun but commercial, il est réalisé dans un objectif pédagogique au sein de l'organisme de formation IFOCOP.</div>

    <p>L’ensemble des éléments (photographies, images et textes) utilisés sur ce site est la propriété de VÉVILLE Location. Le présent site internet est protégé par les lois françaises régissant la propriété intellectuelle.<br>Il est strictement interdit de le reproduire sous quelque forme que ce soit, dans sa forme ou son contenu, totalement ou partiellement, et sur tous supports sans un accord écrit de son auteur.<br>Nous vous informons que VÉVILLE Location autorise tout site internet précisant les mentions obligatoires, ou tout autre support à citer VÉVILLE Location et à mettre en place un lien hypertexte pointant vers la page d’accueil : https://veville.rafox.ovh</p>
       
    <h2 style="text-align:center">Zhaira Dhia</h2>
    <p style="text-align:center">dhia.rafox@gmail.com – 06 86 76 54 07<br>Responsable de publication : Zhaira Dhia<br>Hébergeur : SAS Scaleway - https://scaleway.com </p>
    </div>
  </div>
@endsection
