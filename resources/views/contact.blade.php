@extends('layouts.app')

@section('content')
<div class="container">
    <div class="siimple-content siimple-content--large">
        <div class="siimple-h2 siimple--text-normal siimple--mb-1" align="left">Contact</div>
        @include('partials.notif')
        <form method="POST" action="{{ route('contact') }}">
            @csrf
            <div class="siimple-grid">
                <div class="siimple-grid-row">
                    <div class="siimple-grid-col siimple-grid-col--6
                        siimple-grid-col--sm-12">
                        <label for="email" class="siimple-field-label">{{
                            __('Adresse email') }}</label>


                        <input class="siimple-input siimple-input--fluid {{
                            $errors->has('email') ? 'is-invalid' : '' }}"
                        id="email" type="email" class="form-control{{ $errors->has('email')
                        ? ' is-invalid' : '' }}" name="email" placeholder="@if(Auth::user()) {{ Auth::user()->email }} @endif
" required @if(Auth::user()) disabled @endif>

                    </div>
                    <div class="siimple-grid-col siimple-grid-col--6
                        siimple-grid-col--sm-12">
                        <label for="name_first_name"
                            class="siimple-field-label">{{ __('Nom & Prénom') }}
                        </label>

                        <input id="name_first_name" type="text"
                            class="siimple-input siimple-input--fluid {{
                            $errors->has('name_first_name') ? ' is-invalid' : ''
                        }}" name="name" placeholder="@if(Auth::user()) {{ Auth::user()->name }} {{ Auth::user()->first_name }} @endif"
                        required @if(Auth::user()) disabled @endif>
                    </div>
                    <div class="siimple-grid-col siimple-grid-col--12">
                            <div class="siimple-field">
                            <label for="message" class="siimple-field-label">{{ __('Votre message:') }} </label>
                            <textarea class="siimple-textarea siimple-textarea--fluid" rows="7" name="message" value="{{ old('message') }}"
                            required autofocus></textarea>
                            @if ($errors->has('message'))
                            <span class="siimple-field-helper siimple--color-error"
                                role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                            @endif
                        </div>
                            <div class="siimple-field">
                                    <button type="submit" class="siimple-btn siimple-btn--primary siimple-btn--fluid siimple--text-bold">
                                        {{ __('Envoyer') }}
                                    </button>
                            </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
