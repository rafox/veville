module.exports = Menu = {
    start : function () {   
        const menu_btn = document.querySelector('#menu_btn')
        const menu = document.querySelector('#menu')
        var menu_hidden = true;
        menu_btn.addEventListener("click", function(e) {
            e.stopPropagation()
            e.preventDefault() 
            if (menu_hidden == true) {
                menu.classList.remove('hide')
                menu_btn.classList.add('active')
                menu_hidden = false
            }else{
                menu.classList.add('hide')
                menu_btn.classList.remove('active')
                menu_hidden = true
            }
        })
    
        document.body.addEventListener("click", function (e) {
            if (menu_hidden == false ){
                menu.classList.add('hide')
                menu_btn.classList.remove('active')
                menu_hidden = true
            }
            
        })
    }
}