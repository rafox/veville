<?php

return [
  'admin' => [
    'username' => env('USER_NAME_ADMIN', 'admin'),
    'password' => env('USER_PASSWORD_ADMIN', 'admin'),
    'mail' => env('USER_MAIL_ADMIN')
  ],
  'slave' => [ 
    'username' => env('USER_NAME', 'slave'),
    'password' => env('USER_PASSWORD', 'slave'),
    'mail' => env('USER_MAIL')
  ]
];