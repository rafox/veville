<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * Get agency.
     */
    public function agency()
    {
        return $this->belongsTo('App\Agency', 'agency_id');
    }

    /**
     * Get vmodel.
     */
    public function vmodel()
    {
        return $this->belongsTo('App\Vmodel', 'vmodel_id');
    }

    /**
     * Get orders.
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
}
