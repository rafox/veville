<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vmodel extends Model
{
    /**
    * Get the vehicles for the Vmodel.
    */
    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }

    /**
    * Get the brand for the Vmodel.
    */
    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }
}
