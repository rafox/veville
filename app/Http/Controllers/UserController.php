<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;



class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware('banned');
    }

    /**
     * Show the account form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $id = Auth::user()->id;
        $user = User::find($id);
        return view('account', ["user"=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if(Auth::user()->id != $request->id){
            return redirect()->to('/account')->with('error','Vous ne pouvez pas modifier un autre utilisateur !');

        }else{
            if(Auth::user()->username == $request->username){
                $username_rule = ['required', 'string', 'max:20'];
            }else{
                $username_rule = ['required', 'string', 'max:20', 'unique:users'];
            }
            if(Auth::user()->email == $request->email){
                $email_rule = ['required', 'string', 'email', 'max:50'];
            }else{
                $email_rule = ['required', 'string', 'email', 'max:50', 'unique:users'];
            }

            $this->validate($request, [
                'username' => $username_rule,
                'name' => ['required', 'string', 'max:20'],
                'first_name' => ['required', 'string', 'max:20'],
                'gender' => ['required', 
                    Rule::in(['m', 'w']),
                ],
                'email' => $email_rule,
                'password' => ['confirmed'],
            ]);
            $id = request()->route('id');
            $user = User::find($id);
            $user->username = $request->username;
            $user->name = $request->name;
            $user->first_name = $request->first_name;
            $user->email = $request->email;
            if (trim($request->password) != "") {
                $user->password = $request->password;
            }
            $user->gender = $request->gender;
            $user->save();
            return redirect()->to('/account')->with('success','Utilisateur modifié avec success !');
        }
    }

}
