<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vmodel;


class VmodelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vmodel  $vmodel
     * @return \Illuminate\Http\Response
     */
    public function show(Vmodel $vmodel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vmodel  $vmodel
     * @return \Illuminate\Http\Response
     */
    public function edit(Vmodel $vmodel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vmodel  $vmodel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vmodel $vmodel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vmodel  $vmodel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vmodel $vmodel)
    {
        //
    }
}
