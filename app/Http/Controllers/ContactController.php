<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\ContactAdmin;
use Illuminate\Support\Facades\Mail;




class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('throttle:6,1')->only('send');
        $this->middleware('banned');
    }

    /**
     * Show the application contact form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('contact');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'message' => 'required|max:255',
            'email' => 'required|email',
            'name' => 'required|regex:/^[a-zA-Z ]*$/|max:30'
        ], [
            'name.regex' => ':name ne peut contenir que des lettres et des espaces.'
        ]
        );
    }

    public function send(Request $request)
    {
        if(Auth::user()){
        $data = [
            'message' => $request->post('message'),
            'name'  => Auth::user()->name . " " . Auth::user()->first_name ,
            'email'  => Auth::user()->email,
        ];
        }else{
            $data = [
                'message' => $request->post('message'),
                'name'  =>  $request->post('name'),
                'email'  => $request->post('email'),
            ];
        }
        Mail::to('admin.veville@protonmail.com', 'Véville')
        ->send(new ContactAdmin($data));
        return redirect('/contact')->with('success', 'Votre mail a bien été envoyé.');

    }
}