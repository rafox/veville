<?php

namespace App\Http\Controllers\Admin;

use App\Vmodel;
use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class VmodelController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware('admin');
        $this->middleware('banned');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $vmodels = Vmodel::all();
      return view('admin.vmodels.index', ["vmodels"=>$vmodels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $brands = Brand::all();
      return view('admin.vmodels.create', ["brands" => $brands]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'title' => 'required|alpha|max:60',
        'passagers' => 'required|numeric|min:1|max:9',
        'bags' => 'required|alpha|max:5',
        'doors' => 'required|numeric|min:1|max:5',
        'emission_level' => 'required|numeric|max:250',
        'transmission' => ['required', 
        Rule::in(['m', 'a']),
        ],   
        'brand_id' => 'required|numeric',
        'image' => 'image',
       ]);
      $vmodel = new Vmodel;
      $vmodel->title = strtoupper($request->title);
      $vmodel->passagers = $request->passagers;
      $vmodel->bags = $request->bags;
      $vmodel->doors = $request->doors;
      $vmodel->emission_level = $request->emission_level;
      $vmodel->transmission = $request->transmission;
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/uploads/vmodels');
        $image->move($destinationPath, $input['imagename']);
        $vmodel->photo = '/images/uploads/vmodels/' . $input['imagename'];
      }
      $vmodel->brand_id = $request->brand_id;
      $vmodel->save();
      return redirect()->to('/admin/models')->with('success','Agence crée avec success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vmodel  $vmodel
     * @return \Illuminate\Http\Response
     */
    public function show(Vmodel $vmodel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vmodel  $vmodel
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vmodel $vmodel)
    {
      $id = request()->route('id');
      $vmodel = Vmodel::find($id);
      $brands = Brand::all();
      return view('admin.vmodels.edit', ["vmodel"=>$vmodel, "brands" => $brands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vmodel  $vmodel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vmodel $vmodel)
    {
      $request->validate([
        'title' => 'required|string|max:60',
        'passagers' => 'required|numeric|min:1|max:9',
        'bags' => 'required|alpha|max:5',
        'doors' => 'required|numeric|min:1|max:5',
        'emission_level' => 'required|numeric|max:250',
        'transmission' => ['required', 
        Rule::in(['m', 'a']),
        ],   
        'brand_id' => 'required|numeric',
        'image' => 'image',
       ]);
      $id = request()->route('id');
      $vmodel = Vmodel::find($id);
      $vmodel->title = strtoupper($request->title);
      $vmodel->passagers = $request->passagers;
      $vmodel->bags = $request->bags;
      $vmodel->doors = $request->doors;
      $vmodel->emission_level = $request->emission_level;
      $vmodel->transmission = $request->transmission;
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/uploads/vmodels');
        $image->move($destinationPath, $input['imagename']);
        $vmodel->photo = '/images/uploads/vmodels/' . $input['imagename'];
      }
      $vmodel->brand_id = $request->brand_id;
      $vmodel->save();
      return redirect()->to('/admin/models')->with('success','Agence modifié avec success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vmodel  $vmodel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = request()->route('id');
      $vmodel = Vmodel::destroy($id);
      return redirect()->to('/admin/models')->with('success','Agence supprimé avec success !');

    }
}