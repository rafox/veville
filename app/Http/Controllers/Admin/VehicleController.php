<?php

namespace App\Http\Controllers\Admin;

use App\Vmodel;
use App\Vehicle;
use App\Agency;
use Illuminate\Http\Request;

class VehicleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware('admin');
        $this->middleware('banned');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $vehicles = Vehicle::all();
      return view('admin.vehicles.index', ["vehicles"=>$vehicles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $agencies = Agency::all();
      $vmodels = Vmodel::all();
      return view('admin.vehicles.create', ["agencies" => $agencies, "vmodels" => $vmodels]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'daily_price' => 'required|numeric',
        'vmodel_id' => 'required|numeric',
        'agency_id' => 'required|numeric',
       ]);
      $vehicle = new Vehicle;
      $vehicle->daily_price = $request->daily_price;
      $vehicle->vmodel_id = $request->vmodel_id;
      $vehicle->agency_id = $request->agency_id;
      $vehicle->save();
      return redirect()->to('/admin/vehicles')->with('success','Vehicle crée avec success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vehicle $vehicle)
    {
      $id = request()->route('id');
      $vehicle = Vehicle::find($id);
      $agencies = Agency::all();
      $vmodels = Vmodel::all();
      return view('admin.vehicles.edit', ["vehicle"=>$vehicle, "agencies" => $agencies, "vmodels" => $vmodels]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
      $request->validate([
        'daily_price' => 'required|numeric',
        'vmodel_id' => 'required|numeric',
        'agency_id' => 'required|numeric',
       ]);
      $id = request()->route('id');
      $vehicle = Vehicle::find($id);
      $vehicle->daily_price = $request->daily_price;
      $vehicle->vmodel_id = $request->vmodel_id;
      $vehicle->agency_id = $request->agency_id;
      $vehicle->save();
      return redirect()->to('/admin/vehicles')->with('success','Vehicle modifié avec success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = request()->route('id');
      $vehicle = Vehicle::destroy($id);
      return redirect()->to('/admin/vehicles')->with('success','Vehicle supprimé avec success !');

    }
}