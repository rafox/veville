<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Vehicle;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware('admin');
        $this->middleware('banned');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $orders = Order::all();
      return view('admin.orders.index', ["orders"=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $users = User::all();
      $vehicles = Vehicle::all();
      return view('admin.orders.create', ["users" => $users, "vehicles" => $vehicles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'date_time_departure' => 'required|date',
        'date_time_end' => 'required|date',
        'vehicle_id' => 'required|numeric',
        'user_id' => 'required|numeric',
        'image' => 'image',
       ]);
       if ($request->date_time_departure < date("Y-m-d") ||  $request->date_time_departure > $request->date_time_end) {
        return redirect()->to('/admin/orders')->with('error','Date de départ doit être superieur à la date du jour et date de retour superieur à la date de départ !');
      }
      $orders = new Order;
      $orders->date_time_departure = $request->date_time_departure;
      $orders->date_time_end = $request->date_time_end;
      $diff = strtotime($request->date_time_departure) - strtotime($request->date_time_end); 
      $days = abs(round($diff / 86400));
      $orders->vehicle_id = $request->vehicle_id;
      $vehicle = Vehicle::find($request->vehicle_id);
      $orders->total_price =  $vehicle->daily_price *  $days;
      $orders->user_id = $request->user_id;
      $orders->agency_id = $vehicle->agency_id;
      $orders->save();
      return redirect()->to('/admin/orders')->with('success','Commande crée avec success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $orders
     * @return \Illuminate\Http\Response
     */
    public function show(Order $orders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
      $id = request()->route('id');
      $order = Order::find($id);
      $vehicles = Vehicle::all();
      $users = User::all();
      return view('admin.orders.edit', ["order"=>$order, "users" => $users, "vehicles" => $vehicles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $request->validate([
        'date_time_departure' => 'required|date',
        'date_time_end' => 'required|date',
        'vehicle_id' => 'required|numeric',
        'user_id' => 'required|numeric',
        'image' => 'image',
       ]);

       if ($request->date_time_departure < date("Y-m-d") ||  $request->date_time_departure > $request->date_time_end) {
          return redirect()->to('/admin/orders')->with('error','Date de départ doit être superieur à la date du jour et date de retour superieur à la date de départ !');
        }

      $id = request()->route('id');
      $vehicle = Vehicle::find($request->vehicle_id);
      $order = Order::find($id);
      $order->date_time_departure = $request->date_time_departure;
      $order->date_time_end = $request->date_time_end;
      $diff = strtotime($request->date_time_departure) - strtotime($request->date_time_end); 
      $days = abs(round($diff / 86400));
      $order->vehicle_id = $request->vehicle_id;
      $order->total_price =  $vehicle->daily_price *  $days;
      $order->user_id = $request->user_id;
      $order->agency_id = $vehicle->agency_id;
      $order->save();
      return redirect()->to('/admin/orders')->with('success','Commande modifié avec success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = request()->route('id');
      $order = Order::destroy($id);
      return redirect()->to('/admin/orders')->with('success','Commande supprimé avec success !');

    }
}