<?php

namespace App\Http\Controllers\Admin;

use App\Agency;
use Illuminate\Http\Request;

class AgencyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware('admin');
        $this->middleware('banned');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $agencies = Agency::all();
      return view('admin.agencies.index', ["agencies"=>$agencies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.agencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $request->validate([
        'title' => 'required|string|unique:agencies|max:60',
        'address' => 'required|string|max:80',
        'city' => 'required|string|max:30',
        'zip' => 'required|numeric|max:99999',
        'image' => 'required|image',
    ]);

      $agency = new Agency;
      $agency->title = $request->title;
      $agency->address = $request->address;
      $agency->city = $request->city;
      $agency->zip = $request->zip;
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/uploads/agencies');
        $image->move($destinationPath, $input['imagename']);
        $agency->photo = '/images/uploads/agencies/' . $input['imagename'];
      }
      $agency->save();
      return redirect()->to('/admin/agencies')->with('success','Agence crée avec success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function show(Agency $agency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function edit(Agency $agency)
    {
      $id = request()->route('id');
      $agency = Agency::find($id);
      return view('admin.agencies.edit', ["agency"=>$agency]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agency $agency)
    {
      $request->validate([
        'title' => 'required|string|max:60',
        'address' => 'required|string|max:80',
        'city' => 'required|string|max:30',
        'zip' => 'required|numeric|max:99999',
        'image' => 'image',
       ]);
      $id = request()->route('id');
      $agency = Agency::find($id);
      $agency->title = $request->title;
      $agency->address = $request->address;
      $agency->city = $request->city;
      $agency->zip = $request->zip;
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/uploads/agencies');
        $image->move($destinationPath, $input['imagename']);
        $agency->photo = '/images/uploads/agencies/' . $input['imagename'];
      }
      $agency->save();
      return redirect()->to('/admin/agencies')->with('success','Agence modifié avec success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = request()->route('id');
      $agency = Agency::destroy($id);
      return redirect()->to('/admin/agencies')->with('success','Agence supprimé avec success !');

    }
}