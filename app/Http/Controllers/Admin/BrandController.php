<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware('admin');
        $this->middleware('banned');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $brands = Brand::all();
      return view('admin.brands.index', ["brands"=>$brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'title' => 'required|alpha|unique:brands|max:60',
        'image' => 'required|image',
       ]);
      $brand = new Brand;
      $brand->title = $request->title;
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/uploads/brands');
        $image->move($destinationPath, $input['imagename']);
        $brand->photo = '/images/uploads/brands/' . $input['imagename'];
      }
      $brand->save();
      return redirect()->to('/admin/brands')->with('success','Marque crée avec success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $Brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $Brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $id = request()->route('id');
      $brand = Brand::find($id);
      return view('admin.brands.edit', ["brand"=>$brand]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $request->validate([
        'title' => 'required|alpha|max:60',
        'image' => 'image',
       ]);
      $id = request()->route('id');
      $brand = Brand::find($id);
      $brand->title = $request->title;
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/uploads/brands/');
        $image->move($destinationPath, $input['imagename']);
        $brand->photo = '/images/uploads/brands/' . $input['imagename'];
      }
      $brand->save();
      return redirect()->to('/admin/brands')->with('success','Marque modifié avec success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id = request()->route('id');
      $brand = Brand::destroy($id);
      return redirect()->to('/admin/brands')->with('success','Marque supprimé avec success !');

    }
}