<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware('admin');
        $this->middleware('banned');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', ["users"=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.users.create');
    }


    /**
     * save the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => ['required', 'string', 'max:20', 'unique:users'],
            'name' => ['required', 'string', 'max:20'],
            'first_name' => ['required', 'string', 'max:20'],
            'gender' => ['required', 
                Rule::in(['m', 'w']),
            ],
            'isAdmin' => ['required', 
                Rule::in(['on', 'off']),
            ],
            'isBanned' => ['required', 
                Rule::in(['on', 'off']),
            ],
            'email' => ['required', 'string', 'email', 'max:50', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        $user = new User;
        $user->username = ucfirst($request->username);
        $user->name = ucfirst($request->name);
        $user->first_name = ucfirst($request->first_name);
        $user->email = $request->email;
        $user->password = $request->password;
        $user->gender = $request->gender;
        switch ($request->isAdmin) {
            case "on":
                $user->isAdmin = 1;
                break;
            default:
                $user->isAdmin = 0;
                break;
        }
        switch ($request->isBanned) {
            case "on":
                $user->isBanned = 1;
                break;
            default:
                $user->isBanned = 0;
                break;
        }
        $user->save();
        return redirect()->to('/admin/users/create')->with('success','Utilisateur crée avec success !');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = request()->route('id');
        $user = User::find($id);
        return view('admin.users.edit', ["user"=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $id = request()->route('id');
        $user = User::find($id);
        if($user->username == $request->username){
            $username_rule = ['required', 'string', 'max:20'];
        }else{
            $username_rule = ['required', 'string', 'max:20', 'unique:users'];
        }
        if($user->email == $request->email){
            $email_rule = ['required', 'string', 'email', 'max:50'];
        }else{
            $email_rule = ['required', 'string', 'email', 'max:50', 'unique:users'];
        }
       $this->validate($request, [
        'username' => $username_rule,
        'name' => ['required', 'string', 'max:20'],
        'first_name' => ['required', 'string', 'max:20'],
        'gender' => ['required', 
            Rule::in(['m', 'w']),
        ],
        'isAdmin' => [
            Rule::in(['on', 'off']),
        ],
        'isBanned' => [
            Rule::in(['on', 'off']),
        ],
        'email' => $email_rule,
        'password' => ['confirmed'],
      ]);
      $user->username = $request->username;
      $user->name = $request->name;
      $user->first_name = $request->first_name;
      $user->email = $request->email;
      if (trim($request->password) != "") {
        $user->password = $request->password;
      }
      $user->gender = $request->gender;
      if($request->has('isAdmin')){
        switch ($request->isAdmin) {
            case "on":
                $user->isAdmin = 1;
                break;
            case "off":
                $user->isAdmin = 0;
                break;
            }
        }
        if($request->has('isBanned')){
            switch ($request->isBanned) {
                case "on":
                    $user->isBanned = 1;
                    break;
                case "off":
                    $user->isBanned = 0;
                    break;
            }
        }
      $user->save();
      return redirect()->to('/admin/users')->with('success','Utilisateur modifié avec success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id' => ['exists:users,id'],

          ]);
        $id = request()->route('id');
        $user = User::destroy($id);
        return redirect()->to('/admin/users')->with('success','Utilisateur supprimé avec success !');

    }
}
