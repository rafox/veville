<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agency;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('banned');
    }

    /**
     * Show the application home page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $agencies = Agency::all();
        return view('home', ["agencies" => $agencies]);
    }
    
    public function legales()
    {
        return view('legales');
    }

    public function cgv()
    {
        return view('cgv');
    }
}
