<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Agency;
use App\Vehicle;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class VehicleController extends Controller
{    
    /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware('verified');
       $this->middleware('banned');
   }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource from search.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $request->validate([
            'date_time_departure' => 'date',
            'date_time_end' => 'date',
            'agency_id' => 'numeric',
            'order' => [
            Rule::in(['Desc', 'Asc']),
            ],  
           ]);
           if ($request->date_time_departure < date("Y-m-d") ||  $request->date_time_departure > $request->date_time_end) {
            return redirect()->to('/')->with('error','Date de départ doit être superieur à la date du jour et date de retour superieur à la date de départ !');
          }
        $agencies = Agency::all();
        $list = DB::table('vehicles')
        ->join('orders', 'vehicles.id', '=', 'orders.vehicle_id')
        ->whereNotBetween('orders.date_time_departure', [$request->date_time_departure, $request->date_time_end])
        ->whereNotBetween('orders.date_time_end', [$request->date_time_departure, $request->date_time_end])
        ->where('vehicles.agency_id', $request->agency_id)
        ->select('vehicles.id')
        ->get()->pluck('id')->toArray();

        if(empty($list)){
            return redirect()->to('/')->with('error','Aucun véhicule n\'est disponible !');
        }
        $diff = strtotime($request->date_time_departure) - strtotime($request->date_time_end); 
        $days = abs(round($diff / 86400));
        if($request->order != ""){
            //dd('hello');
            switch ($request->order) {
                case 'Desc':
                        $vehicles = Vehicle::find($list)
                        ->sortByDesc('daily_price');
                    break;
                
                case 'Asc':
                        $vehicles = Vehicle::find($list)
                        ->sortBy('daily_price');
                    break;
            }
        }else{
            $vehicles = Vehicle::find($list)
            ->sortBy('daily_price');
        }

        //dd($vehicles);
         return view('home', ["agencies" => $agencies, 
                              "vehicles" => $vehicles, 
                              "date_time_departure" => $request->date_time_departure,
                              "date_time_end" => $request->date_time_end,
                              "days" => $days,
                              "order" => ["Desc", "Asc"]
                              ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        //
    }
}
