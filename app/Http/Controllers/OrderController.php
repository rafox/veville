<?php

namespace App\Http\Controllers;

use App\Order;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;



class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware('banned');
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comming_orders = Order::where('user_id', Auth::user()->id)
                                ->where('date_time_end', '>', date("Y-m-d"))
                                ->orderBy('created_at', 'desc')->get();
        $previous_orders = Order::where('user_id', Auth::user()->id)
                                ->where('date_time_end', '<', date("Y-m-d"))
                                ->orderBy('created_at', 'desc')->get();
        $last_order = Order::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
                                    
        return view('orders', ["last_order" => $last_order, "comming_orders"=>$comming_orders, "previous_orders" => $previous_orders]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date_time_departure' => 'required|date',
            'date_time_end' => 'required|date',
            'vehicle_id' => 'required|numeric',
           ]);

        if ($request->date_time_departure < date("Y-m-d") ||  $request->date_time_departure > $request->date_time_end) {
            return redirect()->to('/')->with('error','Date de départ doit être superieur à la date du jour et date de retour superieur à la date de départ !');
          }

        $vehicle = Vehicle::find($request->vehicle_id);

        $list = DB::table('vehicles')
        ->join('orders', 'vehicles.id', '=', 'orders.vehicle_id')
        ->whereNotBetween('orders.date_time_departure', [$request->date_time_departure, $request->date_time_end])
        ->whereNotBetween('orders.date_time_end', [$request->date_time_departure, $request->date_time_end])
        ->where('vehicles.id', $vehicle->id)
        ->select('vehicles.id')
        ->get()->pluck('id')->toArray();

        if (empty($list)) {
            return redirect()->to('/')->with('error','Vehicle n\'est pas disponible !');
        }
    
        $order = new Order;
        $order->user_id = Auth::user()->id;
        $order->vehicle_id = $vehicle->id;
        $order->agency_id = $vehicle->agency_id;
        $order->date_time_departure = $request->date_time_departure;
        $order->date_time_end = $request->date_time_end;
        $diff = strtotime($request->date_time_departure) - strtotime($request->date_time_end); 
        $days = abs(round($diff / 86400));
        $order->total_price = $days * $vehicle->daily_price;
        $order->save();
        return redirect()->to('/')->with('success','Commande enregistrée avec success !');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
