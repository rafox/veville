<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
    * Get the vehicles models for the Brand.
    */
    public function vmodels()
    {
        return $this->hasMany('App\Vehicle');
    }

}
