<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    /**
    * Get the vehicles for the Agency.
    */
    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }


}
